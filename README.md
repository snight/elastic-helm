# Elastic Stack Kubernetes Helm Charts

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Charts](#charts)
- [受支持的配置](#受支持的配置)
  - [支持矩阵](#支持矩阵)
  - [Kubernetes版本](#kubernetes版本)
  - [Helm版本](#helm版本)
- [ECK](#eck)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Charts

这些Helm Charts旨在以比较轻量的方式配置官方Docker镜像。下面也添加了相关Docker镜像文档的链接。
我们建议在部署时，Helm Charts版本要与产品版本保持一致，这将确保您使用的Charts版本已经针对相应的生产版本通过了兼容性测试，同时也确保Charts文档和示例可以与您要安装的产品版本能够在一起使用。

例如，如果要部署Elasticsearch 7.10.1集群，请使用相应的 7.10.1 分支版本

master这些Charts版本旨在预发行产品的最新版本，因此可能会出现无法与当前发行版本一起使用的问题。

| Chart                                      | Docker 文档                                                                      | 最新 v7 版本                 |       最新 v6 版本            |
|--------------------------------------------|---------------------------------------------------------------------------------|----------------------------|-----------------------------|
| [APM-Server](./apm-server/README.md)       | https://www.elastic.co/guide/en/apm/server/current/running-on-docker.html       | [`7.10.1`][apm-7]           | [`6.8.13`][apm-6]           |
| [Elasticsearch](./elasticsearch/README.md) | https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html     | [`7.10.1`][elasticsearch-7] | [`6.8.13`][elasticsearch-6] |
| [Filebeat](./filebeat/README.md)           | https://www.elastic.co/guide/en/beats/filebeat/current/running-on-docker.html   | [`7.10.1`][filebeat-7]      | [`6.8.13`][filebeat-6]      |
| [Kibana](./kibana/README.md)               | https://www.elastic.co/guide/en/kibana/current/docker.html                      | [`7.10.1`][kibana-7]        | [`6.8.13`][kibana-6]        |
| [Logstash](./logstash/README.md)           | https://www.elastic.co/guide/en/logstash/current/docker.html                    | [`7.10.1`][logstash-7]      | [`6.8.13`][logstash-6]      |
| [Metricbeat](./metricbeat/README.md)       | https://www.elastic.co/guide/en/beats/metricbeat/current/running-on-docker.html | [`7.10.1`][metricbeat-7]    | [`6.8.13`][metricbeat-6]    |

## 受支持的配置

从7.7.0发行版开始，一些Charts已成为正式发布的版本

注意，仅支持来自 [Elastic Helm repo] 或者 [GitHub releases] 正式发布的Charts。

### 支持矩阵

|     | Elasticsearch | Kibana | Logstash | Filebeat | Metricbeat | APM Server |
|-----|---------------|--------|----------|----------|------------|------------|
| 6.8 | Beta          | Beta   | Beta     | Beta     | Beta       | Alpha      |
| 7.0 | Alpha         | Alpha  | /        | /        | /          | /          |
| 7.1 | Beta          | Beta   | /        | Beta     | /          | /          |
| 7.2 | Beta          | Beta   | /        | Beta     | Beta       | /          |
| 7.3 | Beta          | Beta   | /        | Beta     | Beta       | /          |
| 7.4 | Beta          | Beta   | /        | Beta     | Beta       | /          |
| 7.5 | Beta          | Beta   | Beta     | Beta     | Beta       | Alpha      |
| 7.6 | Beta          | Beta   | Beta     | Beta     | Beta       | Alpha      |
| 7.7 | GA            | GA     | Beta     | GA       | GA         | Beta       |
| 7.8 | GA            | GA     | Beta     | GA       | GA         | Beta       |
| 7.9 | GA            | GA     | Beta     | GA       | GA         | Beta       |

### Kubernetes版本

该Charts目前已针对所有可用的GKE版本进行了测试。确切的版本在`KUBERNETES_VERSIONS`下面的 [helm-tester Dockerfile][] 中有定义。

### Helm版本

检查向后的兼容性时，仅使用 [helm-tester Dockerfile][]（当前为3.4.2）中提到的Helm版本对Charts进行测试。

## ECK

除了这些Helm Charts之外，Elastic还提供了基于 [Operator pattern][]的[Elastic Cloud on Kubernetes][]，并且Elastic推荐以这种方式在Kubernetes上部署Elasticsearch，Kibana和APM Server。这还有一个专用于ECK的Helm Charts，可以在[ECK repo][eck-chart] ([documentation][eck-chart-doc])中找到。


[currently tested]: https://devops-ci.elastic.co/job/elastic+helm-charts+master/
[eck-chart]: https://github.com/elastic/cloud-on-k8s/tree/master/deploy
[eck-chart-doc]: https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-install-helm.html
[elastic cloud on kubernetes]: https://github.com/elastic/cloud-on-k8s
[elastic helm repo]: https://helm.elastic.co
[github releases]: https://github.com/elastic/helm-charts/releases
[helm-tester Dockerfile]: https://github.com/elastic/helm-charts/blob/master/helpers/helm-tester/Dockerfile
[helpers/matrix.yml]: https://github.com/elastic/helm-charts/blob/master/helpers/matrix.yml
[operator pattern]: https://kubernetes.io/docs/concepts/extend-kubernetes/operator/
[apm-7]: https://github.com/elastic/helm-charts/tree/7.10.1/apm-server/README.md
[apm-6]: https://github.com/elastic/helm-charts/tree/6.8.13/apm-server/README.md
[elasticsearch-7]: https://github.com/elastic/helm-charts/tree/7.10.1/elasticsearch/README.md
[elasticsearch-6]: https://github.com/elastic/helm-charts/tree/6.8.13/elasticsearch/README.md
[filebeat-7]: https://github.com/elastic/helm-charts/tree/7.10.1/filebeat/README.md
[filebeat-6]: https://github.com/elastic/helm-charts/tree/6.8.13/filebeat/README.md
[kibana-7]: https://github.com/elastic/helm-charts/tree/7.10.1/kibana/README.md
[kibana-6]: https://github.com/elastic/helm-charts/tree/6.8.13/kibana/README.md
[logstash-7]: https://github.com/elastic/helm-charts/tree/7.10.1/logstash/README.md
[logstash-6]: https://github.com/elastic/helm-charts/tree/6.8.13/logstash/README.md
[metricbeat-7]: https://github.com/elastic/helm-charts/tree/7.10.1/metricbeat/README.md
[metricbeat-6]: https://github.com/elastic/helm-charts/tree/6.8.13/metricbeat/README.md

# 一、环境配置

## Kubernetes 节点信息
|  节点名称  | 节点类型 |CPU 配置|Memory 配置|
|:---------:|:--------:|:-----:|:---------:|
|k8s-master1|  master  |  8C   |    32G    |
|k8s-master2|  master  |  8C   |    32G    |
|k8s-master3|  master  |  8C   |    32G    |
| k8s-node1 |  worker  |  8C   |    32G    |
| k8s-node2 |  worker  |  8C   |    32G    |
| k8s-node3 |  worker  |  8C   |    32G    |

## Elasticsearch 集群环境信息
ElasticSearch 安装有最低安装要求，如果执行 Helm 安装命令后 Pod 无法正常启动，请检查是否符合最低要求的配置。

|   集群名称   | 节点类型 |副本数量|存储大小|    网络模式    |CPU最小要求|内存最小要求|                描述                |
|:-----------:|:--------:|:-----:|:-----:|:-------------:|:--------:|:---------:|------------------------------------|
|elasticsearch|  master  |   3   |   5G  |   ClusterIP   | 核心数 > 2| 内存 > 2G |主节节点，用于控制 ES 集群            |
|elasticsearch|   data   |   3   |  50G  |   ClusterIP   | 核心数 > 1| 内存 > 2G |数据节点，用于存储 ES 数据            |
|elasticsearch|  client  |   2   |   无  |NodePort(30002)| 核心数 > 1| 内存 > 2G |负责处理用户请求，实现请求转发、负载均衡|

**注意**：nodes节点数要小于分片副本数，否则会出现 unassigned shards 问题。

## Kibana/Filebeat/Logstash 环境信息
|  应用名称  | 副本数量 | 存储大小 |   网络模式   |               描述               |
|:---------:|:--------:|:-----:|:-------------:|----------------------------------|
|   Kibana  |     1    |   无  |NodePort(30003)|展示 ElasticSearch 数据的应用      |
|  Filebeat |     1    |   无  |               |收集各节点的日志文件                |
|  Logstash |     1    |   1G  |   ClusterIP   |过滤日志文件内容转发给 ElasticSearch|

# 二、配置持久化存储卷
配置一个 SotrageClass，用于创建 Kubernetes 存储用的 PV、PVC，因为 ElasticSearch 部署的是 StatefulSet 类型资源，涉及到自动卷分配，需要一个存储卷分配服务。例如，使用 NFS 存储就需要 NFS 共享网络卷的 NFS-Provisioner 服务，能够帮我们自动创建存储空间及 PV 与 PVC，请确认 Kubernetes 集群中存在这样的卷分配服务。

## 1、安装Helm 3.4.2
```shell
# 下载helm二进制文件
curl -O https://get.helm.sh/helm-v3.4.2-linux-amd64.tar.gz
tar -zxvf helm-v3.4.2-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/helm
# 添加helm仓库
helm repo add apphub https://apphub.aliyuncs.com
helm repo add elastic https://helm.elastic.co
```
## 2、创建NFS存储类
|    IP地址   |    端口   |   服务   |
|-------------|:--------:|:--------:|
|192.168.1.100| 111,2029 |NFS Server|
```shell
# 安装 nfs-client-provisioner
helm install nfs-storage -n default \
--set nfs.server=192.168.1.100,nfs.path=/data/NFS \
--set storageClass.name=nfs-storage,storageClass.reclaimPolicy=Retain \
apphub/nfs-client-provisioner
```
- nfs.server：指定 nfs 服务地址
- nfs.path：指定 nfs 对应目录
- storageClass.name：此配置用于绑定 PVC 和 PV
- storageClass.reclaimPolicy：回收策略，默认是删除
- -n：命名空间

# 三、创建ES集群证书
## 1、生成证书文件
```shell
# 创建生成证书的脚本
cat > es-security.sh << EOF
#!/bin/bash
# 指定 elasticsearch 版本
RELEASE=7.10.1
# 运行容器生成证书
docker run --name elastic-charts-certs -i -w /app \
  elasticsearch:${RELEASE} \
  /bin/sh -c " \
    elasticsearch-certutil ca --out /app/elastic-stack-ca.p12 --pass '' && \
    elasticsearch-certutil cert --name security-master --dns security-master --ca /app/elastic-stack-ca.p12 --pass '' --ca-pass '' --out /app/elastic-certificates.p12" && \
# 从容器中将生成的证书拷贝出来
docker cp elastic-charts-certs:/app/elastic-certificates.p12 ./ && \
  # 证书生成成功该容器删除
  docker rm -f elastic-charts-certs && \
  openssl pkcs12 -nodes -passin pass:'' -in elastic-certificates.p12 -out elastic-certificate.pem
EOF

# 执行脚本，在当前目录生成证书
chmod +x es-security.sh
./es-security.sh
```
## 2、添加证书到 Kubernetes
```shell
# 创建命名空间efk
kubectl create ns efk
# 添加证书
kubectl create secret -n efk generic elastic-certificates --from-file=elastic-certificates.p12
kubectl create secret -n efk generic elastic-certificate-pem --from-file=elastic-certificate.pem

# 设置集群用户名密码，用户名不建议修改
kubectl create secret -n efk generic elastic-credentials \
  --from-literal=username=elastic --from-literal=password=password

```

# 四、配置应用参数
通过 Helm 安装 ElasticSearch、Kibana，需要事先创建一个带有配置参数的 values.yaml 文件。然后再执行 Helm install 安装命令时，指定使用此文件。
下载本项目代码仓库
```shell
git clone https://gitlab.com/snight/elastic-helm.git
```
## ElasticSearch 安装的配置文件
- elastic-helm/elasticsearch/es-master-values.yaml
- elastic-helm/elasticsearch/es-data-values.yaml
- elastic-helm/elasticsearch/es-client-values.yaml

## Kibana 安装的配置文件
- elastic-helm/kibana/kibana-values.yaml

## Filebeat 安装的配置文件
- elastic-helm/filebeat/filebeat-values.yaml

## Logstash 安装的配置文件
- elastic-helm/logstash/logstash-values.yaml

# 五、使用Helm部署EFK
## 1、Helm 安装 ElasticSearch
ElasticSearch 安装部署如下：
```shell
# 进入 elasticsearch 目录
cd elastic-helm/elasticsearch

# 安装 ElasticSearch Master 节点
helm install elasticsearch-master -f es-master-values.yaml --namespace efk --version 7.10.1 ./

# 安装 ElasticSearch Data 节点
helm install elasticsearch-data -f es-data-values.yaml --namespace efk --version 7.10.1 ./

# 安装 ElasticSearch Client 节点
helm install elasticsearch-client -f es-client-values.yaml --namespace efk --version 7.10.1 ./
```
- -f：指定部署配置文件
- --version：指定使用的 Helm Chart 版本号
- --namespace：指定部署应用的 Namespace 空间
>在安装 Master 节点后 Pod 启动时候会抛出异常，就绪探针探活失败，这是个正常现象。在执行安装 Data 节点后 Master 节点 Pod 就会恢复正常。

## 2、Helm 安装 Kibana
```shell
# 进入 kibana 目录
cd elastic-helm/kibana

# 安装 Kibana
helm install kibana -f kibana-values.yaml --namespace efk --version 7.10.1 ./
```

## 3、Helm 安装 Filebeat
```shell
# 进入 kibana 目录
cd elastic-helm/filebeat

# 安装 Kibana
helm install filebeat -f filebeat-values.yaml --namespace efk --version 7.10.1 ./
```

## 4、Helm 安装 Logstash
```shell
# 进入 kibana 目录
cd elastic-helm/logstash

# 安装 Kibana
helm install logstash -f logstash-values.yaml --namespace efk --version 7.10.1 ./
```

# 六、访问 Kibana 浏览数据日志
由上面我们指定了 Kibana 的 NodePort 端口为 30003 ,所以这里我们输入 `http://<K8S集群IP地址>:30003` ，即可访问 Kibana 界面。这时，在弹出的登录窗口中输入上面配置的用户名、密码进行登录：<br>
- 用户名: elastic
- 密  码: password

